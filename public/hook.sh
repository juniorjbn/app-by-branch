#!/bin/bash

URL=https://hooks.slack.com/services/xxxxxxxxx/xxxxxxxxx/xxxxxxxxxxx

curl -v -XPOST --data-urlencode payload="{'channel': '#melissa-site', 'username': 'GetupBOT', 'text': 'Deploy Finalizado com Sucesso para "${OPENSHIFT_BUILD_NAME}" ("${OPENSHIFT_DEPLOYMENT_NAMESPACE}") :octocat: Commit: "${GIT_MSG}"', 'icon_emoji': ':rocket:'}" $URL

echo
